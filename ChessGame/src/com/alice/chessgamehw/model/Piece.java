package com.alice.chessgamehw.model;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;

public abstract class Piece {
	private String name;
	private char currentRowPosition;
	private int currentColumnPosition;
	private GameColor color;
	private List<Character> possibleRowMoves;
	private List<Integer> possibleColumnMoves;
	private boolean isChecked = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getCurrentRowPosition() {
		return currentRowPosition;
	}
	public void setCurrentRowPosition(char currentRowPosition) {
		this.currentRowPosition = currentRowPosition;
	}
	public int getCurrentColumnPosition() {
		return currentColumnPosition;
	}
	public void setCurrentColumnPosition(int currentColumnPosition) {
		this.currentColumnPosition = currentColumnPosition;
	}
	public GameColor getColor() {
		return color;
	}
	public void setColor(GameColor color) {
		this.color = color;
	}
	public List<Character> getPossibleRowMoves() {
		return possibleRowMoves;
	}
	public void setPossibleRowMoves(List<Character> possibleRowMoves) {
		this.possibleRowMoves = possibleRowMoves;
	}
	public List<Integer> getPossibleColumnMoves() {
		return possibleColumnMoves;
	}
	public void setPossibleColumnMoves(List<Integer> possibleColumnMoves) {
		this.possibleColumnMoves = possibleColumnMoves;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
}
