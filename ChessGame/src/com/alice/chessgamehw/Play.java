package com.alice.chessgamehw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.board.impl.BoardServiceImpl;
import com.alice.chessgamehw.service.move.impl.MoveServiceImpl;
import com.alice.chessgamehw.service.piece.impl.PieceServiceImpl;

public class Play {

	private List<Piece> selectablePieces;
	private Piece selectedPiece;

	private List<Piece> pieces;
	private HashMap<String, Piece> squares;

	MoveServiceImpl moveService;
	PieceServiceImpl pieceService;
	BoardServiceImpl boardService;

	public Play(List<Piece> pieces, HashMap<String, Piece> squares) {
		this.pieces = pieces;
		this.squares = squares;
		this.moveService = new MoveServiceImpl();
		this.pieceService = new PieceServiceImpl();
		this.boardService = new BoardServiceImpl();
	}

	public void startGame() throws InterruptedException, IOException {
		System.out.println("");
		System.out.println("**************************************************");
		System.out.println(" Pick a Piece to Move in order to Start the Game!");
		System.out.println("**************************************************");
		System.out.println("");
		System.out.println(" Pieces you can move: ");
		System.out.println("--------------------------");

		whiteTurn();
	}

	private void whiteTurn() throws IOException, InterruptedException {

		selectablePieces = pieceService.getSelectablePieces(pieces, GameColor.WHITE);

		pieceService.printSelectablePieces(selectablePieces);

		int chosenNumber = getChoise(selectablePieces.size());

		selectedPiece = selectablePieces.get(chosenNumber - 1);

		moveService.printSelectableMoves(selectedPiece);

		makeMoveForWhite();
	}

	private void makeMoveForWhite() throws InterruptedException, IOException {

		var oldRow = selectedPiece.getCurrentRowPosition();
		var oldColumn = selectedPiece.getCurrentColumnPosition();

		int chosenNumber = getChoise(selectedPiece.getPossibleRowMoves().size());

		selectedPiece = moveService.makeMove(selectedPiece, chosenNumber, oldRow, oldColumn, " You Moved");

		pieces = pieceService.handlePieceTakeAnother(pieces, squares, selectedPiece);

		squares = boardService.updateSquares(squares, oldRow, oldColumn, selectedPiece);

		pieces = pieceService.updatePieces(pieces, squares);

		blackTurn();
	}

	private void blackTurn() throws InterruptedException, IOException {
		System.out.println("");
		System.out.print(" Computer is thinking");
		for (int i = 0; i < 3; i++) {
			TimeUnit.SECONDS.sleep(1);
			System.out.print(".");
		}
		System.out.println("");

		selectablePieces = pieceService.getSelectablePieces(pieces, GameColor.BLACK);

		int randomNumber = getRandom(selectablePieces.size());
		selectedPiece = selectablePieces.get(randomNumber - 1);

		makeMoveForBlack();
	}

	private void makeMoveForBlack() throws InterruptedException, IOException {

		var oldRow = selectedPiece.getCurrentRowPosition();
		var oldColumn = selectedPiece.getCurrentColumnPosition();

		int randomNumber = getRandom(selectedPiece.getPossibleRowMoves().size());

		selectedPiece = moveService.makeMove(selectedPiece, randomNumber, oldRow, oldColumn, "Computer Moved");

		pieces = pieceService.handlePieceTakeAnother(pieces, squares, selectedPiece);

		squares = boardService.updateSquares(squares, oldRow, oldColumn, selectedPiece);

		pieces = pieceService.updatePieces(pieces, squares);

		whiteTurn();
	}

	private int getChoise(int max) throws NumberFormatException, IOException {
		System.out.println("--------------------------");

		System.out.print(" Your Choise: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int choise;
		do {
			choise = Integer.parseInt(br.readLine());
			if (choise > max) {
				printWarning(String.format("Please select number between 1 and %d", max));

				System.out.print(" Choose Again: ");
			}
		} while (choise > max);
			
		return choise;
	}

	private int getRandom(int max) {
		return ThreadLocalRandom.current().nextInt(1, max + 1);
	}
	
	private void printWarning(String message) {
		System.out.println(String.format(" %s ", message));
	}
}
