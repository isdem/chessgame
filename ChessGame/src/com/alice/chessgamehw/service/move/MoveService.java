package com.alice.chessgamehw.service.move;

import java.util.List;

import com.alice.chessgamehw.model.Piece;

public interface MoveService {
	
	Piece makeMove(Piece selectedPiece, int chosenNumber, char oldRow, int oldColumn, String message);
	void printSelectableMoves(Piece selectedPiece);
	void printSelectablePieces(List<Piece> selectablePieces);
	void printMoveToConsole(Piece selectedPiece, String message, char oldRow, int oldColumn);
}
