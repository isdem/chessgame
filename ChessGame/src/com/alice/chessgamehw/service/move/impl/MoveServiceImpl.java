package com.alice.chessgamehw.service.move.impl;

import java.util.List;

import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.move.MoveService;
import com.alice.chessgamehw.service.piece.impl.PieceServiceImpl;

public class MoveServiceImpl implements MoveService {
	
	PieceServiceImpl pieceService;
	
	public MoveServiceImpl() {
		this.pieceService = new PieceServiceImpl();
	}

	@Override
	public void printSelectableMoves(Piece selectedPiece) {
		
		System.out.println("");
		System.out.println(" Your Possible Moves:");
		System.out.println("--------------------------");
		
		for (int i = 0; i < selectedPiece.getPossibleRowMoves().size(); i++) {
			System.out.println(
					String.format(" %d) %s-%d", 
							i + 1,
							selectedPiece.getPossibleRowMoves().get(i), 
							selectedPiece.getPossibleColumnMoves().get(i)));
		}
	}

	@Override
	public void printSelectablePieces(List<Piece> selectablePieces) {
		int count = 0;

		for (Piece piece : selectablePieces) {
			count++;
			System.out.println(String.format(" %d) %s | in | %s-%d |", count, piece.getName(),
					piece.getCurrentRowPosition(), piece.getCurrentColumnPosition()));
		}
	}
	
	@Override
	public void printMoveToConsole(Piece selectedPiece, String message, char oldRow, int oldColumn) {
		System.out.println("");
		System.out.println("*************************************");
		System.out.println(String.format(" %s %s-%d %s to %s-%d", message, oldRow, oldColumn, selectedPiece.getName(),
				selectedPiece.getCurrentRowPosition(), selectedPiece.getCurrentColumnPosition()));

		System.out.println("*************************************");
		System.out.println("");
	}
	
	@Override
	public Piece makeMove(Piece selectedPiece, int chosenNumber, char oldRow, int oldColumn, String message) {

		var newRow = selectedPiece.getPossibleRowMoves().get(chosenNumber - 1);
		var newColumn = selectedPiece.getPossibleColumnMoves().get(chosenNumber - 1);		

		selectedPiece.setCurrentRowPosition(newRow);
		selectedPiece.setCurrentColumnPosition(newColumn);
		
		printMoveToConsole(selectedPiece, message, oldRow, oldColumn);
		
		return selectedPiece;
	}
}
