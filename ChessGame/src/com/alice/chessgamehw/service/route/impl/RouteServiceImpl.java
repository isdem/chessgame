package com.alice.chessgamehw.service.route.impl;

import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.King;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.route.RouteService;

public class RouteServiceImpl implements RouteService {

	/*
	 * Defines until where the piece can move as straight line
	 * Rook and Queen use this format
	 */
	@Override
	public Piece lineRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns) {
		char nextRowValue = (char) (piece.getCurrentRowPosition() + 1);
		char previousRowValue = (char) (piece.getCurrentRowPosition() - 1);

		for (int i = piece.getCurrentColumnPosition() - 1; i >= 1; i--) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i)) {
				break;
			}
		}
		for (int i = piece.getCurrentColumnPosition() + 1; i <= 8; i++) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i)) {
				break;
			}
		}
		for (char i = previousRowValue; i >= 'A'; i--) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i)) {
				break;
			}
		}
		for (char i = nextRowValue; i <= 'H'; i++) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i)) {
				break;
			}
		}

		return piece;
	}

	/*
	 * Defines until where the piece can move as crosswise
	 * Bishop and Queen use this format
	 */
	@Override
	public Piece crossRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns) {
		char nextRowValue = (char) (piece.getCurrentRowPosition() + 1);
		char previousRowValue = (char) (piece.getCurrentRowPosition() - 1);

		int column = piece.getCurrentColumnPosition();
		for (char i = nextRowValue; i <= 'H'; i++) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i, ++column)) {
				break;
			}
		}
		column = piece.getCurrentColumnPosition();
		for (char i = previousRowValue; i >= 'A'; i--) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i, --column)) {
				break;
			}
		}
		column = piece.getCurrentColumnPosition();
		for (char i = previousRowValue; i >= 'A'; i--) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i, ++column)) {
				break;
			}
		}
		column = piece.getCurrentColumnPosition();
		for (char i = nextRowValue; i <= 'H'; i++) {
			if (!addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, i, --column)) {
				break;
			}
		}

		return piece;
	}
	
	/*
	 * Queen can move in this two format
	 */
	@Override
	public Piece crossAndLineRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns) {
		lineRoutes(squares, piece, possibleRows, possibleColumns);
		crossRoutes(squares, piece, possibleRows, possibleColumns);
		
		return piece;
	}	
	
	@Override 
	public Piece kingRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns) {
		char nextRowValue = (char) (piece.getCurrentRowPosition() + 1);
		char previousRowValue = (char) (piece.getCurrentRowPosition() - 1);

		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns,
				piece.getCurrentColumnPosition() - 1);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns,
				piece.getCurrentColumnPosition() + 1);

		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue);

		int column = piece.getCurrentColumnPosition();
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue, ++column);

		column = piece.getCurrentColumnPosition();
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue, --column);

		column = piece.getCurrentColumnPosition();
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue, ++column);

		column = piece.getCurrentColumnPosition();
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue, --column);

		return piece;
	}
	
	@Override 
	public Piece knightRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns) {
		char nextRowValue = (char) (piece.getCurrentRowPosition() + 1);
		char previousRowValue = (char) (piece.getCurrentRowPosition() - 1);

		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue,
				piece.getCurrentColumnPosition() + 2);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue,
				piece.getCurrentColumnPosition() + 2);

		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue,
				piece.getCurrentColumnPosition() - 2);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue,
				piece.getCurrentColumnPosition() - 2);
		
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, (char) (nextRowValue + 1),
				piece.getCurrentColumnPosition() + 1);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, (char) (previousRowValue - 1),
				piece.getCurrentColumnPosition() + 1);
		
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, (char) (nextRowValue + 1),
				piece.getCurrentColumnPosition() - 1);
		addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, (char) (previousRowValue - 1),
				piece.getCurrentColumnPosition() - 1);

		return piece;
	}

	@Override
	public boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, int column) {

		if (column <= 0 || column >= 9) {
			return false;
		}
		
		var route = String.format("%s%d", piece.getCurrentRowPosition(), column);
		Piece pieceOnTheRoute = squares.get(route);
		if (pieceOnTheRoute == null) {
			addRoute(piece, possibleRows, possibleColumns, column);
			return true;
		} else if (pieceOnTheRoute.getColor() != piece.getColor()) {
			addRoute(piece, possibleRows, possibleColumns, column);
			checkWarning(pieceOnTheRoute);
			return false;
		} else {
			return false;
		}
	}

	@Override
	public boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, char row, int column) {

		if (column <= 0 || column >= 9 || row > 'H' || row < 'A') {
			return false;
		}

		var route = String.format("%s%d", row, column);
		Piece pieceOnTheRoute = squares.get(route);
		if (pieceOnTheRoute == null) {
			addRoute(piece, possibleRows, possibleColumns, row, column);
			return true;
		} else if (pieceOnTheRoute.getColor() != piece.getColor()) {
			addRoute(piece, possibleRows, possibleColumns, row, column);
			checkWarning(pieceOnTheRoute);
			return false;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, char row) {

		if (row > 'H' || row < 'A') {
			return false;
		}

		var route = String.format("%s%d", row, piece.getCurrentColumnPosition());
		Piece pieceOnTheRoute = squares.get(route);
		if (pieceOnTheRoute == null) {
			addRoute(piece, possibleRows, possibleColumns, row, piece.getCurrentColumnPosition());
			return true;
		} else if (pieceOnTheRoute.getColor() != piece.getColor()) {
			addRoute(piece, possibleRows, possibleColumns, row, piece.getCurrentColumnPosition());
			return false;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isThereAnyRivalPiece(HashMap<String, Piece> squares, GameColor color, char row, int column) {
		var route = String.format("%s%d", row, column);
		Piece pieceOnTheRoute = squares.get(route);
		if (pieceOnTheRoute != null && pieceOnTheRoute.getColor() != color) {
			return true;
		}
		return false;
	}

	private void addRoute(Piece piece, List<Character> possibleRows, List<Integer> possibleColumns, int column) {
		possibleColumns.add(column);
		possibleRows.add(piece.getCurrentRowPosition());
		piece.setPossibleRowMoves(possibleRows);
		piece.setPossibleColumnMoves(possibleColumns);
	}

	private void addRoute(Piece piece, List<Character> possibleRows, List<Integer> possibleColumns, char row,
			int column) {
		possibleColumns.add(column);
		possibleRows.add(row);
		piece.setPossibleRowMoves(possibleRows);
		piece.setPossibleColumnMoves(possibleColumns);
	}
	
	private void checkWarning(Piece pieceOnTheRoute) {
		if (pieceOnTheRoute instanceof King) {
			pieceOnTheRoute.setChecked(true);
			System.out.println("");
			System.out.println("***************************************");
			System.out.println("");
			
			if (pieceOnTheRoute.getColor() == GameColor.BLACK) {
				System.out.println(" You checked rival King!!!");
			}
			else {
				System.out.println(" Your opponent checked your King!!!");
			}
			
			System.out.println("");
			System.out.println("***************************************");
			System.out.println("");
		}
	}
}
