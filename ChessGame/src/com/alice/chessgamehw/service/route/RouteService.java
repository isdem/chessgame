package com.alice.chessgamehw.service.route;

import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;

public interface RouteService {
	
	boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, int column);

	Piece lineRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns);

	Piece crossRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns);

	Piece crossAndLineRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns);

	boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, char row, int column);
	
	boolean isThereAnyRivalPiece(HashMap<String, Piece> squares, GameColor color, char row, int column);
	
	boolean addNewPossibleRoutes(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns, char row);
	
	Piece kingRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns);
	
	Piece knightRoute(HashMap<String, Piece> squares, Piece piece, List<Character> possibleRows,
			List<Integer> possibleColumns);
}
