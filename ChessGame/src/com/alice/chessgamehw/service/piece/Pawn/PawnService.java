package com.alice.chessgamehw.service.piece.Pawn;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Pawn;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class PawnService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var pawn = new Pawn();
		pawn.setColor(color);
		pawn.setCurrentRowPosition(row);
		pawn.setCurrentColumnPosition(column);
		pawn.setName(name);
		pawn.setPossibleRowMoves(possibleMoveRows);
		pawn.setPossibleColumnMoves(possibleMoveColumns);
		return pawn;
	}
}
