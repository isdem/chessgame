package com.alice.chessgamehw.service.piece.Pawn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.route.impl.RouteServiceImpl;

public class WhitePawnServiceImpl extends PawnService {

	RouteServiceImpl routeService;

	public WhitePawnServiceImpl() {
		this.routeService = new RouteServiceImpl();
	}

	@Override
	public Piece updatePiece(Piece piece, HashMap<String, Piece> squares) {
		List<Integer> possibleColumns = new ArrayList<Integer>();
		List<Character> possibleRows = new ArrayList<Character>();
		
		piece.setPossibleRowMoves(null);
		piece.setPossibleColumnMoves(null);

		char nextRowValue = (char) (piece.getCurrentRowPosition() + 1);
		char previousRowValue = (char) (piece.getCurrentRowPosition() - 1);

		routeService.addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns,
				piece.getCurrentColumnPosition() + 1);

		if (piece.getCurrentColumnPosition() == 2) {
			routeService.addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns,
					piece.getCurrentColumnPosition() + 2);
		}

		if (routeService.isThereAnyRivalPiece(squares, piece.getColor(), nextRowValue, piece.getCurrentColumnPosition() + 1)) {
			routeService.addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, nextRowValue,
					piece.getCurrentColumnPosition() + 1);
		}
		if (routeService.isThereAnyRivalPiece(squares, piece.getColor(), previousRowValue, piece.getCurrentColumnPosition() + 1)) {
			routeService.addNewPossibleRoutes(squares, piece, possibleRows, possibleColumns, previousRowValue,
					piece.getCurrentColumnPosition() + 1);
		}
		
		return piece;
	}
}
