package com.alice.chessgamehw.service.piece;

import java.util.HashMap;

import com.alice.chessgamehw.model.Piece;

public interface PieceEditingService {

	Piece updatePiece(Piece piece, HashMap<String, Piece> squares);
}
