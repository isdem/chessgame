package com.alice.chessgamehw.service.piece.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Bishop;
import com.alice.chessgamehw.model.King;
import com.alice.chessgamehw.model.Knight;
import com.alice.chessgamehw.model.Pawn;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.model.Queen;
import com.alice.chessgamehw.model.Rook;
import com.alice.chessgamehw.service.piece.PieceService;
import com.alice.chessgamehw.service.piece.King.BlackKingServiceImpl;
import com.alice.chessgamehw.service.piece.King.KingService;
import com.alice.chessgamehw.service.piece.King.WhiteKingServiceImpl;
import com.alice.chessgamehw.service.piece.Pawn.BlackPawnServiceImpl;
import com.alice.chessgamehw.service.piece.Pawn.PawnService;
import com.alice.chessgamehw.service.piece.Pawn.WhitePawnServiceImpl;
import com.alice.chessgamehw.service.piece.bishop.BishopService;
import com.alice.chessgamehw.service.piece.bishop.BlackBishopServiceImpl;
import com.alice.chessgamehw.service.piece.bishop.WhiteBishopServiceImpl;
import com.alice.chessgamehw.service.piece.knight.BlackKnightServiceImpl;
import com.alice.chessgamehw.service.piece.knight.KnightService;
import com.alice.chessgamehw.service.piece.knight.WhiteKnightServiceImpl;
import com.alice.chessgamehw.service.piece.queen.BlackQueenServiceImpl;
import com.alice.chessgamehw.service.piece.queen.QueenService;
import com.alice.chessgamehw.service.piece.queen.WhiteQueenServiceImpl;
import com.alice.chessgamehw.service.piece.rook.BlackRookServiceImpl;
import com.alice.chessgamehw.service.piece.rook.RookService;
import com.alice.chessgamehw.service.piece.rook.WhiteRookServiceImpl;
import com.alice.chessgamehw.service.route.impl.RouteServiceImpl;

public class PieceServiceImpl implements PieceService {

	PawnService blackPawnService;
	PawnService whitePawnService;
	RookService blackRookService;
	RookService whiteRookService;
	KnightService blackKnightService;
	KnightService whiteKnightService;
	BishopService blackBishopService;
	BishopService whiteBishopService;
	QueenService blackQueenService;
	QueenService whiteQueenService;
	KingService blackKingService;
	KingService whiteKingService;
	RouteServiceImpl routeService;

	public PieceServiceImpl() {
		blackPawnService = new BlackPawnServiceImpl();
		whitePawnService = new WhitePawnServiceImpl();
		blackRookService = new BlackRookServiceImpl();
		whiteRookService = new WhiteRookServiceImpl();
		blackKnightService = new BlackKnightServiceImpl();
		whiteKnightService = new WhiteKnightServiceImpl();
		blackBishopService = new BlackBishopServiceImpl();
		whiteBishopService = new WhiteBishopServiceImpl();
		blackQueenService = new BlackQueenServiceImpl();
		whiteQueenService = new WhiteQueenServiceImpl();
		blackKingService = new BlackKingServiceImpl();
		whiteKingService = new WhiteKingServiceImpl();
		this.routeService = new RouteServiceImpl();
	}

	@Override
	public List<Piece> getSelectablePieces(List<Piece> pieces, GameColor color) {
		var selectablePieces = pieces.stream()
				.filter(f -> f.getColor().equals(color) && f.getPossibleRowMoves() != null)
				.collect(Collectors.toList());

		return selectablePieces;
	}

	@Override
	public void printSelectablePieces(List<Piece> selectablePieces) {
		int count = 0;

		for (Piece piece : selectablePieces) {
			count++;
			System.out.println(String.format(" %d) %s | in | %s-%d |", count, piece.getName(),
					piece.getCurrentRowPosition(), piece.getCurrentColumnPosition()));
		}
	}

	@Override
	public List<Piece> handlePieceTakeAnother(List<Piece> pieces, HashMap<String, Piece> squares, Piece selectedPiece) {
		if (routeService.isThereAnyRivalPiece(squares, selectedPiece.getColor(), selectedPiece.getCurrentRowPosition(),
				selectedPiece.getCurrentColumnPosition())) {
			String key = String.format("%s%d", selectedPiece.getCurrentRowPosition(),
					selectedPiece.getCurrentColumnPosition());
			var enemyPiece = squares.get(key);
			pieces.remove(enemyPiece);
		}
		return pieces;
	}

	public List<Piece> updatePieces(List<Piece> pieces, HashMap<String, Piece> squares) {
		List<Piece> updatedPieces = new ArrayList<Piece>();
		for (Piece piece : pieces) {
			if (piece instanceof Rook) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whiteRookService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackRookService.updatePiece(piece, squares));
				}
			} else if (piece instanceof Knight) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whiteKnightService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackKnightService.updatePiece(piece, squares));
				}
			} else if (piece instanceof Bishop) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whiteBishopService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackBishopService.updatePiece(piece, squares));
				}
			} else if (piece instanceof Queen) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whiteQueenService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackQueenService.updatePiece(piece, squares));
				}
			} else if (piece instanceof King) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whiteKingService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackKingService.updatePiece(piece, squares));
				}
			} else if (piece instanceof Pawn) {
				if (piece.getColor() == GameColor.WHITE) {
					updatedPieces.add(whitePawnService.updatePiece(piece, squares));
				} else {
					updatedPieces.add(blackPawnService.updatePiece(piece, squares));
				}
			}
		}
		return updatedPieces;
	}
}
