package com.alice.chessgamehw.service.piece;

import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;

public interface PieceService {
	
	void printSelectablePieces(List<Piece> selectablePieces);
	List<Piece> getSelectablePieces(List<Piece> pieces, GameColor color);
	List<Piece> handlePieceTakeAnother(List<Piece> pieces, HashMap<String, Piece> squares, Piece selectedPiece);
	List<Piece> updatePieces(List<Piece> pieces, HashMap<String, Piece> squares);
}
