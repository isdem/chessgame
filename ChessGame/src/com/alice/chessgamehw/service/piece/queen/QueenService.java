package com.alice.chessgamehw.service.piece.queen;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.model.Queen;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class QueenService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var queen = new Queen();
		queen.setColor(color);
		queen.setCurrentRowPosition(row);
		queen.setCurrentColumnPosition(column);
		queen.setName(name);
		queen.setPossibleRowMoves(possibleMoveRows);
		queen.setPossibleColumnMoves(possibleMoveColumns);
		return queen;
	}
}
