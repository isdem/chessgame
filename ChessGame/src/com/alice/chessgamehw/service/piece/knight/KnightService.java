package com.alice.chessgamehw.service.piece.knight;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Knight;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class KnightService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var knight = new Knight();
		knight.setColor(color);
		knight.setCurrentRowPosition(row);
		knight.setCurrentColumnPosition(column);
		knight.setName(name);
		knight.setPossibleRowMoves(possibleMoveRows);
		knight.setPossibleColumnMoves(possibleMoveColumns);
		return knight;
	}
}
