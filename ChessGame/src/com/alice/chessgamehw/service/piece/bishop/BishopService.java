package com.alice.chessgamehw.service.piece.bishop;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Bishop;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class BishopService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var bishop = new Bishop();
		bishop.setColor(color);
		bishop.setCurrentRowPosition(row);
		bishop.setCurrentColumnPosition(column);
		bishop.setName(name);
		bishop.setPossibleRowMoves(possibleMoveRows);
		bishop.setPossibleColumnMoves(possibleMoveColumns);
		return bishop;
	}
}
