package com.alice.chessgamehw.service.piece.rook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.route.impl.RouteServiceImpl;

public class WhiteRookServiceImpl extends RookService {

	RouteServiceImpl routeService;

	public WhiteRookServiceImpl() {
		this.routeService = new RouteServiceImpl();
	}

	@Override
	public Piece updatePiece(Piece piece, HashMap<String, Piece> squares) {
		List<Integer> possibleColumns = new ArrayList<Integer>();
		List<Character> possibleRows = new ArrayList<Character>();
		
		piece.setPossibleRowMoves(null);
		piece.setPossibleColumnMoves(null);

		return routeService.lineRoutes(squares, piece, possibleRows, possibleColumns);
	}
}
