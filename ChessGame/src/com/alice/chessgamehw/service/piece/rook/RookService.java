package com.alice.chessgamehw.service.piece.rook;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.model.Rook;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class RookService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var rook = new Rook();
		rook.setColor(color);
		rook.setCurrentRowPosition(row);
		rook.setCurrentColumnPosition(column);
		rook.setName(name);
		rook.setPossibleRowMoves(possibleMoveRows);
		rook.setPossibleColumnMoves(possibleMoveColumns);
		return rook;
	}
}
