package com.alice.chessgamehw.service.piece.King;

import java.util.List;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.King;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.piece.PieceEditingService;

public abstract class KingService implements PieceEditingService {

	public Piece initializePiece(GameColor color, char row, int column, String name, List<Character> possibleMoveRows,
			List<Integer> possibleMoveColumns) {
		var king = new King();
		king.setColor(color);
		king.setCurrentRowPosition(row);
		king.setCurrentColumnPosition(column);
		king.setName(name);
		king.setPossibleRowMoves(possibleMoveRows);
		king.setPossibleColumnMoves(possibleMoveColumns);
		return king;
	}
}
