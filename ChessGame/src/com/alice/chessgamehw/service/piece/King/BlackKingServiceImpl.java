package com.alice.chessgamehw.service.piece.King;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.route.impl.RouteServiceImpl;

public class BlackKingServiceImpl extends KingService {

	RouteServiceImpl routeService;

	public BlackKingServiceImpl() {
		this.routeService = new RouteServiceImpl();
	}

	@Override
	public Piece updatePiece(Piece piece, HashMap<String, Piece> squares) {
		List<Integer> possibleColumns = new ArrayList<Integer>();
		List<Character> possibleRows = new ArrayList<Character>();
		
		piece.setPossibleRowMoves(null);
		piece.setPossibleColumnMoves(null);

		return routeService.kingRoute(squares, piece, possibleRows, possibleColumns);
	}
}
