package com.alice.chessgamehw.service.board.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.alice.chessgamehw.enumeration.GameColor;
import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.board.BoardService;
import com.alice.chessgamehw.service.piece.King.BlackKingServiceImpl;
import com.alice.chessgamehw.service.piece.King.KingService;
import com.alice.chessgamehw.service.piece.King.WhiteKingServiceImpl;
import com.alice.chessgamehw.service.piece.Pawn.BlackPawnServiceImpl;
import com.alice.chessgamehw.service.piece.Pawn.PawnService;
import com.alice.chessgamehw.service.piece.Pawn.WhitePawnServiceImpl;
import com.alice.chessgamehw.service.piece.bishop.BishopService;
import com.alice.chessgamehw.service.piece.bishop.BlackBishopServiceImpl;
import com.alice.chessgamehw.service.piece.bishop.WhiteBishopServiceImpl;
import com.alice.chessgamehw.service.piece.knight.BlackKnightServiceImpl;
import com.alice.chessgamehw.service.piece.knight.KnightService;
import com.alice.chessgamehw.service.piece.knight.WhiteKnightServiceImpl;
import com.alice.chessgamehw.service.piece.queen.BlackQueenServiceImpl;
import com.alice.chessgamehw.service.piece.queen.QueenService;
import com.alice.chessgamehw.service.piece.queen.WhiteQueenServiceImpl;
import com.alice.chessgamehw.service.piece.rook.BlackRookServiceImpl;
import com.alice.chessgamehw.service.piece.rook.RookService;
import com.alice.chessgamehw.service.piece.rook.WhiteRookServiceImpl;

public class BoardServiceImpl implements BoardService {

	PawnService blackPawnService;
	PawnService whitePawnService;
	RookService blackRookService;
	RookService whiteRookService;
	KnightService blackKnightService;
	KnightService whiteKnightService;
	BishopService blackBishopService;
	BishopService whiteBishopService;
	QueenService blackQueenService;
	QueenService whiteQueenService;
	KingService blackKingService;
	KingService whiteKingService;

	public BoardServiceImpl() {
		blackPawnService = new BlackPawnServiceImpl();
		whitePawnService = new WhitePawnServiceImpl();
		blackRookService = new BlackRookServiceImpl();
		whiteRookService = new WhiteRookServiceImpl();
		blackKnightService = new BlackKnightServiceImpl();
		whiteKnightService = new WhiteKnightServiceImpl();
		blackBishopService = new BlackBishopServiceImpl();
		whiteBishopService = new WhiteBishopServiceImpl();
		blackQueenService = new BlackQueenServiceImpl();
		whiteQueenService = new WhiteQueenServiceImpl();
		blackKingService = new BlackKingServiceImpl();
		whiteKingService = new WhiteKingServiceImpl();
	}
 
	@Override
	public List<Piece> initializeBoardForBlack(List<Piece> pieces) {
		pieces.add(blackRookService.initializePiece(GameColor.BLACK, 'A', 8, "Black A Rook", null, null));

		pieces.add(blackKnightService.initializePiece(GameColor.BLACK, 'B', 8, "Black B Knight", Arrays.asList('A', 'C'),
				Arrays.asList(6, 6)));

		pieces.add(blackBishopService.initializePiece(GameColor.BLACK, 'C', 8, "Black C Bishop", null, null));

		pieces.add(blackQueenService.initializePiece(GameColor.BLACK, 'D', 8, "Black Queen", null, null));

		pieces.add(blackKingService.initializePiece(GameColor.BLACK, 'E', 8, "Black King", null, null));

		pieces.add(blackBishopService.initializePiece(GameColor.BLACK, 'F', 8, "Black F Bishop", null, null));

		pieces.add(blackKnightService.initializePiece(GameColor.BLACK, 'G', 8, "Black G Knight", Arrays.asList('F', 'H'),
				Arrays.asList(6, 6)));

		pieces.add(blackRookService.initializePiece(GameColor.BLACK, 'H', 8, "Black H Rook", null, null));

		for (char i = 'A'; i <= 'H'; i++) {
			pieces.add(blackPawnService.initializePiece(GameColor.BLACK, i, 7, "Black Pawn", Arrays.asList(i, i),
					Arrays.asList(6, 5)));
		}

		return pieces;
	}

	@Override
	public List<Piece> initializeBoardForWhite(List<Piece> pieces) {
		pieces.add(whiteRookService.initializePiece(GameColor.WHITE, 'A', 1, "White A Rook", null, null));

		pieces.add(whiteKnightService.initializePiece(GameColor.WHITE, 'B', 1, "White B Knight", Arrays.asList('A', 'C'),
				Arrays.asList(3, 3)));

		pieces.add(whiteBishopService.initializePiece(GameColor.WHITE, 'C', 1, "White C Bishop", null, null));

		pieces.add(whiteQueenService.initializePiece(GameColor.WHITE, 'D', 1, "White Queen", null, null));

		pieces.add(whiteKingService.initializePiece(GameColor.WHITE, 'E', 1, "White King", null, null));

		pieces.add(whiteBishopService.initializePiece(GameColor.WHITE, 'F', 1, "White F Bishop", null, null));

		pieces.add(whiteKnightService.initializePiece(GameColor.WHITE, 'G', 1, "White G Knight", Arrays.asList('F', 'H'),
				Arrays.asList(3, 3)));

		pieces.add(whiteRookService.initializePiece(GameColor.WHITE, 'H', 1, "White H Rook", null, null));

		for (char i = 'A'; i <= 'H'; i++) {
			pieces.add(whitePawnService.initializePiece(GameColor.WHITE, i, 2, "White Pawn", Arrays.asList(i, i),
					Arrays.asList(3, 4)));
		}

		return pieces;
	}

	@Override
	public HashMap<String, Piece> inizializeSquares(List<Piece> pieces) {
		HashMap<String, Piece> pieceMap = new HashMap<String, Piece>();
		for (char i = 'A'; i <= 'H'; i++) {
			char row = i;
			for (int j = 1; j <= 8; j++) {
				int column = j;
				var id = String.format("%s%d", i, j);

				Optional<Piece> piece = pieces.stream()
						.filter(f -> f.getCurrentRowPosition() == row && f.getCurrentColumnPosition() == column)
						.findAny();
				if (piece.isEmpty()) {
					pieceMap.put(id, null);
				} else {
					pieceMap.put(id, piece.get());
				}
			}
		}
		return pieceMap;
	}

	public HashMap<String, Piece> updateSquares(HashMap<String, Piece> squares, char oldRow, int oldColumn,
			Piece selectedPiece) {
		String oldKey = String.format("%s%d", oldRow, oldColumn);
		squares.put(oldKey, null);

		String newKey = String.format("%s%d", selectedPiece.getCurrentRowPosition(),
				selectedPiece.getCurrentColumnPosition());
		squares.put(newKey, selectedPiece);
		return squares;
	}
}
