package com.alice.chessgamehw.service.board;

import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;

public interface BoardService {
	
	public List<Piece> initializeBoardForWhite(List<Piece> pieces);
	public List<Piece> initializeBoardForBlack(List<Piece> pieces);
	public HashMap<String, Piece> inizializeSquares(List<Piece> pieces);
	HashMap<String, Piece> updateSquares(HashMap<String, Piece> squares, char oldRow, int oldColumn, Piece selectedPiece);
}
