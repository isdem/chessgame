package com.alice.chessgamehw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;
import com.alice.chessgamehw.service.board.impl.BoardServiceImpl;

public class Board {
	
	private List<Piece> pieces = new ArrayList<Piece>();
	private HashMap<String, Piece> pieceMap = new HashMap<String, Piece>();
	
	BoardServiceImpl boardService;
	
	public Board() {
		boardService = new BoardServiceImpl();
	}
	
	public List<Piece> getPieces() {
		return pieces;
	}

	public HashMap<String, Piece> getPieceMap() {
		return pieceMap;
	}

	public void initialize(){
		initializePieces();
		initializeSquares();
	}
	
	private void initializePieces() {
		pieces = boardService.initializeBoardForBlack(pieces);
		pieces = boardService.initializeBoardForWhite(pieces);
	}

	private void initializeSquares() {
		pieceMap = boardService.inizializeSquares(pieces);
	}
}
