package com.alice.chessgamehw;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.alice.chessgamehw.model.Piece;

public class Game {
	public static void main(String[] args) {
		Board board = new Board();
		board.initialize();
		
		List<Piece> pieces = board.getPieces();
		HashMap<String, Piece> pieceMap = board.getPieceMap();
		
		Play game = new Play(pieces, pieceMap);
		
		try {
			game.startGame();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong. Tray again!");
		} catch (IOException e) {
			System.out.println("Something went wrong. Tray again!");
		}
	}
}
